<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class EmailNoticationController extends Controller
{
    public function storeMessage(Request $request) {
        $rules = [
            'message' => 'required',
            'name' => 'required',
            'email' => 'required',
            'subject' => 'required',
        ];
        $validator = Validator::make($rules, []);

        if ($validator->fails()) {
            return $this->apiResponse(JsonResponse::HTTP_UNPROCESSABLE_ENTITY, 'message', $validator->errors());
        }

        try {
            DB::beginTransaction();
            $data = [
                'message' => $request->message,
                'name' => $request->name,
                'email' => $request->email,
                'subject' => $request->subject,
            ];
            Message::create($data);
            $data['text'] = $request->message;
            Mail::send('email.message', $data, function($message) use ($data){
                $message->to($data['email'], '')->subject
                ($data['subject']);
                $message->from('info@adscro.com','Advertiser nest');
            });
            DB::commit();
            return $this->apiResponse(JsonResponse::HTTP_OK, 'message', 'Message sent successfully');
        } catch ( \Exception $e) {
            DB::rollBack();
            return $this->apiResponse(JsonResponse::HTTP_INTERNAL_SERVER_ERROR, 'message', $e->getMessage());
        }

    }
}
